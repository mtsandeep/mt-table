const path = require("path");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
  entry: 'rootPath/index.js',
  output: {
    filename: 'dist/bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [
          "rootPath"
        ],
        loader: "babel-loader",
        options: {
          presets: ["es2015"]
        }
      },

      {
        test: "\.html$",

        use: [
          // apply multiple loaders and options
          "htmllint-loader",
          {
            loader: "html-loader",
            options: {
             /* ... */
            }
          }
        ]
      }
    ]
  },

  resolve: {
    modules: [
      "node_modules",
      "rootPath"
    ],
    // directories where to look for modules

    extensions: [".js", ".json", ".jsx", ".css"],
    // extensions that are used

    alias: {
      rootPath: path.resolve(__dirname, 'src')
    }
  },

  // plugins: [
  //   new UglifyJSPlugin()
  // ]
}
